'use strict';

const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const eslint = require('gulp-eslint');

gulp.task('lint', () => {
  return gulp.src('public/js/es6/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('default', ['lint'], () => {
  return gulp.src('public/js/es6/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015'],
    }))
    .pipe(concat('mazeAll.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/js/dist'));
});