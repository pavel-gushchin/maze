class Prim {
  constructor(maze) {
    this.maze = maze;

    this.locations = new Array(maze.cellsInRow + 1);
    for (let i = 0; i <= maze.cellsInRow; i++) {
      this.locations[i] = new Array(maze.cellsInColumn + 1);
      for (let j = 0; j <= maze.cellsInColumn; j++) {
        this.locations[i][j] = 'Outside';
      }
    }

    this.borders = [];
    this.adjacentInsiders = [];

    this.dx = [0, 1, 0, -1];
    this.dy = [-1, 0, 1, 0];
  }

  generateMaze() {
    this.maze.initialize();
    this.maze.showGrid();

    const randX = Prim.randomNumber(this.maze.cellsInRow - 1);
    const randY = Prim.randomNumber(this.maze.cellsInColumn - 1);
    this.locations[randX][randY] = 'Inside';

    this.processCell(randX, randY);

    while (this.borders.length !== 0) {
      this.adjacentInsiders = [];

      const randElement = Prim.randomNumber(this.borders.length);
      const coords = this.borders.splice(randElement, 1);
      const x = coords[0][0];
      const y = coords[0][1];

      this.locations[x][y] = 'Inside';

      this.processCell(x, y);

      if (this.adjacentInsiders.length !== 0) {
        const randAdjacentElement = Prim.randomNumber(this.adjacentInsiders.length);
        const adjacentCoords = this.adjacentInsiders.splice(randAdjacentElement, 1);

        this.destroyWallBetweenCells(x, y, adjacentCoords[0][0], adjacentCoords[0][1]);
      }
    }

    this.maze.drawer.updateCanvas();
    this.maze.drawer.showStartAndFinish();
  }


  processCell(x, y) {
    for (let i = 0; i < 4; i++) {
      const adjacentCellX = x + this.dx[i];
      const adjacentCellY = y + this.dy[i];

      if (adjacentCellX < 0 || adjacentCellY < 0 ||
          adjacentCellX >= this.maze.cellsInRow ||
          adjacentCellY >= this.maze.cellsInColumn) {
        continue;
      }

      switch (this.locations[adjacentCellX][adjacentCellY]) {
        case 'Outside':
          this.locations[adjacentCellX][adjacentCellY] = 'Border';
          this.borders.push([adjacentCellX, adjacentCellY]);
          break;
        case 'Inside':
          this.adjacentInsiders.push([adjacentCellX, adjacentCellY]);
          break;
        default:
          break;
      }
    }
  }

  destroyWallBetweenCells(x, y, xAdjacent, yAdjacent) {
    const dx = x - xAdjacent;
    const dy = y - yAdjacent;

    if (dy === 0) {
      if (dx === 1) {
        this.maze.cells[x][y].leftWall = false;
        this.maze.drawer.eraseWallBetweenCells(x, y, 'left');
      } else if (dx === -1) {
        this.maze.cells[xAdjacent][yAdjacent].leftWall = false;
        this.maze.drawer.eraseWallBetweenCells(xAdjacent, yAdjacent, 'left');
      }
    } else if (dx === 0) {
      if (dy === 1) {
        this.maze.cells[x][y].upperWall = false;
        this.maze.drawer.eraseWallBetweenCells(x, y, 'upper');
      } else if (dy === -1) {
        this.maze.cells[xAdjacent][yAdjacent].upperWall = false;
        this.maze.drawer.eraseWallBetweenCells(xAdjacent, yAdjacent, 'upper');
      }
    }
  }

  static randomNumber(range) {
    return Math.floor(Math.random() * range);
  }
}
