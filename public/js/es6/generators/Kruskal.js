class Kruskal {
  constructor(maze) {
    this.maze = maze;

    this.walls = [];
    for (let i = 0; i < maze.cellsInRow; i++) {
      for (let j = 0; j < maze.cellsInColumn; j++) {
        if (i > 0) {
          this.walls.push({
            x: i,
            y: j,
            type: 'left',
          });
        }

        if (j > 0) {
          this.walls.push({
            x: i,
            y: j,
            type: 'upper',
          });
        }
      }
    }

    this.dx = [0, 1, 0, -1];
    this.dy = [-1, 0, 1, 0];

    this.paths = [];

    this.locations = new Array(maze.cellsInRow);
    for (let i = 0; i < maze.cellsInRow; i++) {
      this.locations[i] = new Array(maze.cellsInColumn);
      for (let j = 0; j < maze.cellsInColumn; j++) {
        this.locations[i][j] = i * this.maze.cellsInColumn + j;
        this.paths.push([
          {
            x: i,
            y: j,
          },
        ]);
      }
    }

    this.cellsToProcess = [];
  }

  generateMaze() {
    this.maze.initialize();
    this.maze.showGrid();

    this.shuffle(this.walls);

    this.walls.forEach((wall) => {
      switch (wall.type) {
        case 'left':
          if (!this.canGo(wall.x, wall.y, wall.x - 1, wall.y)) {
            this.maze.cells[wall.x][wall.y].leftWall = false;
            this.maze.drawer.eraseWallBetweenCells(wall.x, wall.y, 'left');
          }
          break;
        case 'upper':
          if (!this.canGo(wall.x, wall.y, wall.x, wall.y - 1)) {
            this.maze.cells[wall.x][wall.y].upperWall = false;
            this.maze.drawer.eraseWallBetweenCells(wall.x, wall.y, 'upper');
          }
          break;
        default: break;
      }
    });

    this.maze.drawer.updateCanvas();
    this.maze.drawer.showStartAndFinish();
  }

  canGo(startX, startY, finishX, finishY) {
    const startPathNum = this.locations[startX][startY];
    const finishPathNum = this.locations[finishX][finishY];

    if (startPathNum === finishPathNum) {
      return true;
    }

    const startAmountCellsInPath = this.paths[startPathNum].length;
    const finishAmountCellsInPath = this.paths[finishPathNum].length;

    if (startAmountCellsInPath > finishAmountCellsInPath) {
      this.combinePaths(startPathNum, finishPathNum);
    } else {
      this.combinePaths(finishPathNum, startPathNum);
    }

    return false;
  }

  combinePaths(long, short) {
    this.paths[short].forEach((cell) => {
      this.locations[cell.x][cell.y] = long;
      this.paths[long].push({
        x: cell.x,
        y: cell.y,
      });
    });

    this.paths[short] = [];
  }

  shuffle(array) {
    let currentIndex = array.length;

    while (currentIndex !== 0) {
      const randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      const temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
}
