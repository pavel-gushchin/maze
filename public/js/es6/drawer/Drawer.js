class Drawer {
  constructor(maze) {
    this.maze = maze;

    paper.install(window);
    paper.setup(document.getElementById('mainCanvas'));
  }

  eraseWallBetweenCells(x, y, position) {
    const cellSize = this.maze.cellSize;
    const borderSize = this.maze.borderSize;

    let topLeft;
    let rectSize;

    switch (position) {
      case 'left':
        topLeft = new Point(x * cellSize, y * cellSize + borderSize);
        rectSize = new Size(borderSize, cellSize - borderSize);
        break;
      case 'upper':
        topLeft = new Point(x * cellSize + borderSize, y * cellSize);
        rectSize = new Size(cellSize - borderSize, borderSize);
        break;
      default:
        break;
    }

    const rect = new Shape.Rectangle(topLeft, rectSize);
    rect.fillColor = 'white';
  }

  showGrid() {
    for (let x = 0; x < this.maze.width; x += this.maze.cellSize) {
      const topLeft = new Point(x, 0);
      const rectSize = new Size(this.maze.borderSize, this.maze.height);

      const rect = new Shape.Rectangle(topLeft, rectSize);

      rect.fillColor = 'black';
    }

    for (let y = 0; y < this.maze.height; y += this.maze.cellSize) {
      const topLeft = new Point(0, y);
      const rectSize = new Size(this.maze.width, this.maze.borderSize);

      const rect = new Shape.Rectangle(topLeft, rectSize);

      rect.fillColor = 'black';
    }

    this.updateCanvas();
  }

  showStartAndFinish(color = 'white') {
    const cellSize = this.maze.cellSize;
    const borderSize = this.maze.borderSize;
    const startPoint = this.maze.startPoint;

    const startTopLeft = new Point(0, startPoint.y * cellSize + borderSize);
    const startSize = new Size(borderSize, cellSize - borderSize);

    const start = new Shape.Rectangle(startTopLeft, startSize);
    start.fillColor = color;


    const finishPoint = this.maze.finishPoint;
    const finishTopLeft = new Point(
      cellSize * (finishPoint.x + 1),
      cellSize * finishPoint.y + borderSize
    );
    const finishSize = new Size(borderSize, cellSize - borderSize);

    const finish = new Shape.Rectangle(finishTopLeft, finishSize);
    finish.fillColor = color;

    this.updateCanvas();
  }

  showSolution(path, color) {
    const borderSize = this.maze.borderSize;
    const cellSize = this.maze.cellSize;

    path.forEach((cell, index) => {
      const checkedCell = this.maze.cells[cell.x][cell.y];
      let nextCellIndex = index + 1;
      let previousCellIndex = index - 1;

      if (nextCellIndex >= path.length) {
        nextCellIndex = path.length - 1;
      }

      if (previousCellIndex < 0) {
        previousCellIndex = 0;
      }

      let topLeftX;
      let topLeftY;
      let sizeX;
      let sizeY;

      if (checkedCell.leftWall === false &&
          (cell.x - path[nextCellIndex].x === 1 &&
           cell.y - path[nextCellIndex].y === 0 ||
           cell.x - path[previousCellIndex].x === 1 &&
           cell.y - path[previousCellIndex].y === 0)
      ) {
        topLeftX = cellSize * cell.x;
        sizeX = cellSize;
      } else {
        topLeftX = cellSize * cell.x + borderSize;
        sizeX = cellSize - borderSize;
      }

      if (checkedCell.upperWall === false &&
          (cell.x - path[nextCellIndex].x === 0 &&
           cell.y - path[nextCellIndex].y === 1 ||
           cell.x - path[previousCellIndex].x === 0 &&
           cell.y - path[previousCellIndex].y === 1)
      ) {
        topLeftY = cellSize * cell.y;
        sizeY = cellSize;
      } else {
        topLeftY = cellSize * cell.y + borderSize;
        sizeY = cellSize - borderSize;
      }


      const filledCellLeftTop = new Point(topLeftX, topLeftY);
      const filledCellSize = new Size(sizeX, sizeY);
      const filledCell = new Shape.Rectangle(filledCellLeftTop, filledCellSize);
      filledCell.fillColor = color;
    });

    this.showStartAndFinish(color);

    this.updateCanvas();
  }

  updateCanvas() {
    paper.view.draw();
  }
}
