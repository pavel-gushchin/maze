'use strict';

$(document).ready(() => {
  maze.showGrid();
});

$('#prim').click(() => {
  const sizeOfMaze = $('#size').val();
  maze = new Maze(sizeOfMaze);

  if ($('#random_start').is(':checked')) {
    maze.setRandomStart();
  }

  if ($('#random_finish').is(':checked')) {
    maze.setRandomFinish();
  }

  const prim = new Prim(maze);
  prim.generateMaze();
});

$('#kruskal').click(() => {
  const sizeOfMaze = $('#size').val();
  maze = new Maze(sizeOfMaze);

  if ($('#random_start').is(':checked')) {
    maze.setRandomStart();
  }

  if ($('#random_finish').is(':checked')) {
    maze.setRandomFinish();
  }

  const kruskal = new Kruskal(maze);
  kruskal.generateMaze();
});

$('#finder').click(() => {
  const color = $('#color').val();
  const finder = new Finder(maze);
  finder.findExit();
  maze.showSolution(color);
});

$('#size').change(() => {
  const sizeOfMaze = $('#size').val();
  maze = new Maze(sizeOfMaze);
  maze.showGrid();
});
