class Finder {
  constructor(maze, color) {
    this.maze = maze;
    this.color = color;

    this.locations = new Array(maze.cellsInRow);
    for (let i = 0; i < maze.cellsInRow; i++) {
      this.locations[i] = new Array(maze.cellsInColumn);
      for (let j = 0; j < maze.cellsInColumn; j++) {
        this.locations[i][j] = 0;
      }
    }

    this.cellsToProcess = [];
    this.currentNumber = 1;
    this.visited = 0;
  }

  findExit() {
    const startPoint = this.maze.startPoint;
    this.locations[startPoint.x][startPoint.y] = this.currentNumber;
    this.cellsToProcess.push({
      x: startPoint.x,
      y: startPoint.y,
    });

    const mazeSize = this.maze.cellsInRow * this.maze.cellsInColumn;
    let exitIsFound = false;

    while (exitIsFound === false && mazeSize > this.visited) {
      this.currentNumber++;

      const numberOfCellsToProcess = this.cellsToProcess.length;

      for (let i = 0; i < numberOfCellsToProcess; i++) {
        const cell = this.cellsToProcess.shift();

        if (cell.x === this.maze.finishPoint.x &&
            cell.y === this.maze.finishPoint.y
        ) {
          exitIsFound = true;
          break;
        }

        const up = cell.y - 1;
        const right = cell.x + 1;
        const down = cell.y + 1;
        const left = cell.x - 1;

        if (up >= 0 &&
            this.locations[cell.x][up] === 0 &&
            this.maze.cells[cell.x][cell.y].upperWall === false
        ) {
          this.locations[cell.x][up] = this.currentNumber;
          this.cellsToProcess.push({
            x: cell.x,
            y: up,
          });
        }

        if (right < this.maze.cellsInRow &&
            this.locations[right][cell.y] === 0 &&
            this.maze.cells[right][cell.y].leftWall === false
        ) {
          this.locations[right][cell.y] = this.currentNumber;
          this.cellsToProcess.push({
            x: right,
            y: cell.y,
          });
        }

        if (down < this.maze.cellsInColumn &&
            this.locations[cell.x][down] === 0 &&
            this.maze.cells[cell.x][down].upperWall === false
        ) {
          this.locations[cell.x][down] = this.currentNumber;
          this.cellsToProcess.push({
            x: cell.x,
            y: down,
          });
        }

        if (left >= 0 &&
            this.locations[left][cell.y] === 0 &&
            this.maze.cells[cell.x][cell.y].leftWall === false
        ) {
          this.locations[left][cell.y] = this.currentNumber;
          this.cellsToProcess.push({
            x: left,
            y: cell.y,
          });
        }
      }

      this.visited++;
    }


    if (exitIsFound) {
      const pathToExit = [];

      const cell = {
        x: this.maze.finishPoint.x,
        y: this.maze.finishPoint.y,
      };

      pathToExit.push({
        x: cell.x,
        y: cell.y,
      });

      let nextCellWithNumber = this.locations[cell.x][cell.y];

      while (nextCellWithNumber--) {
        const up = cell.y - 1;
        const right = cell.x + 1;
        const down = cell.y + 1;
        const left = cell.x - 1;

        if (up >= 0 &&
          this.locations[cell.x][up] === nextCellWithNumber &&
          this.maze.cells[cell.x][cell.y].upperWall === false
        ) {
          cell.y = up;
          pathToExit.push({
            x: cell.x,
            y: up,
          });
        } else if (right < this.maze.cellsInRow &&
          this.locations[right][cell.y] === nextCellWithNumber &&
          this.maze.cells[right][cell.y].leftWall === false
        ) {
          cell.x = right;
          pathToExit.push({
            x: right,
            y: cell.y,
          });
        } else if (down < this.maze.cellsInColumn &&
          this.locations[cell.x][down] === nextCellWithNumber &&
          this.maze.cells[cell.x][down].upperWall === false
        ) {
          cell.y = down;
          pathToExit.push({
            x: cell.x,
            y: down,
          });
        } else if (left >= 0 &&
          this.locations[left][cell.y] === nextCellWithNumber &&
          this.maze.cells[cell.x][cell.y].leftWall === false
        ) {
          cell.x = left;
          pathToExit.push({
            x: left,
            y: cell.y,
          });
        }
      }

      this.maze.solution = pathToExit;
    } else {
      throw new Error('Can not find exit!');
    }
  }
}
