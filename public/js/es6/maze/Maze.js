class Maze {
  constructor(cellSize = 30, width = 900, height = 600) {
    this.cellSize = parseInt(cellSize, 10);
    this.borderSize = Math.round(this.cellSize / 10);
    this.width = width + this.borderSize;
    this.height = height + this.borderSize;

    this.cellsInRow = Math.round(width / cellSize);
    this.cellsInColumn = Math.round(height / cellSize);

    this.cells = [];
    this.solution = [];
    this.drawer = new Drawer(this);

    this.startPoint = {
      x: 0,
      y: 0,
    };
    this.finishPoint = {
      x: this.cellsInRow - 1,
      y: this.cellsInColumn - 1,
    };
  }

  initialize() {
    this.cells = new Array(this.cellsInRow + 1);
    for (let i = 0; i <= this.cellsInRow; i++) {
      this.cells[i] = new Array(this.cellsInColumn + 1);
      for (let j = 0; j <= this.cellsInColumn; j++) {
        this.cells[i][j] = new Cell();
      }
    }
  }

  show() {
    this.drawer.showMaze();
  }

  showGrid() {
    this.drawer.showGrid();
  }

  showSolution(color) {
    this.drawer.showSolution(this.solution, color);
  }

  setRandomStart() {
    this.startPoint.y = Math.floor(Math.random() * this.cellsInColumn);
  }

  setRandomFinish() {
    this.finishPoint.y = Math.floor(Math.random() * this.cellsInColumn);
  }
}

let maze = new Maze();
