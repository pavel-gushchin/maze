class Cell {
  constructor(lWall = true, uWall = true) {
    this.lWall = lWall;
    this.uWall = uWall;
  }

  get leftWall() {
    return this.lWall;
  }

  set leftWall(lWall) {
    this.lWall = lWall;
  }

  get upperWall() {
    return this.uWall;
  }

  set upperWall(uWall) {
    this.uWall = uWall;
  }
}
